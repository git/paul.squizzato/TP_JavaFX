package Launcher;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import model.Capteur;
import model.CapteurSimple;
import model.StratGenerationAleatoire;
import view.FXMLwindow;
import view.VueCapteur;
import view.VueCapteurSimple;

public class Launch extends Application {
    @Override
    public void start(Stage stage) throws Exception {
        Capteur capti = new CapteurSimple("id5","capteur1", new StratGenerationAleatoire());
        FXMLwindow fenetre = new VueCapteurSimple(capti);

    }
}
