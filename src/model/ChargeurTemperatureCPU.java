package model;

import java.io.BufferedReader;
import java.io.FileReader;

public class ChargeurTemperatureCPU {
    public String load(){
        try {
            BufferedReader fichier = new BufferedReader(new FileReader("/sys/class/thermal/thermal_zone2/temp"));
            return fichier.readLine();
        }catch (Exception ex){
            ex.printStackTrace();
            return null;
        }
    }
}
