package model;

public class StratGenerationCPU extends StratGeneration{
    private ChargeurTemperatureCPU chargeur = new ChargeurTemperatureCPU();
    int generer(){
        String lecture=chargeur.load();
        if(lecture!=null){
            return Integer.parseInt(lecture)/1000;
        }
        return 0;
    }
}
