package model;

import java.util.Random;

public class StratGenerationSeuil extends StratGeneration{
    private DegreKelvin min=new DegreKelvin(0);
    private DegreKelvin max=new DegreKelvin(40);

    @Override
    int generer() {
        Random random = new Random();
        return min.toCelsius()+random.nextInt(max.toCelsius()- min.toCelsius());
    }
}
