package model;

public class DegreKelvin {
    private double valeur;
    public DegreKelvin(double d){
        valeur=d;
    }
    public int toCelsius(){
        return (int)(valeur-273.15);
    }
}
