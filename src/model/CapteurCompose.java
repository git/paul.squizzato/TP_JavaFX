package model;

import javafx.beans.Observable;

import java.util.HashMap;
import java.util.concurrent.atomic.AtomicInteger;

public class CapteurCompose extends Capteur{
    private HashMap<Capteur,Integer> zone;
    private void ajoutCapteur(Capteur c, int poids){
        zone.put(c,poids);
    }
    @Override
    protected int genererTemperature() {
        AtomicInteger temp= new AtomicInteger();
        AtomicInteger poidsT= new AtomicInteger();
        zone.forEach((capt,poids)->{
            temp.addAndGet((capt.getValeur() / poids));
            poidsT.addAndGet(poids);
        });
        return temp.get()/poidsT.get();
    }

}
