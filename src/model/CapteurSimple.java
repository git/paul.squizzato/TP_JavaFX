package model;

import javafx.beans.InvalidationListener;
import javafx.beans.Observable;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.Property;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;

public class CapteurSimple extends Capteur{
    private StratGeneration strategie;
    private void setStrategie(StratGeneration s){
        strategie=s;
    }
    public CapteurSimple(String i, String n,StratGeneration s){
        setStrategie(s);
        id=new SimpleStringProperty();
        nom=new SimpleStringProperty();
        setId(i);
        setNom(n);
        valeur=new SimpleStringProperty();
        tpsGen=new SimpleIntegerProperty();
        setValeur(0);
        setTpsGen(1);
        /*
        strategie=new StratGenerationCPU();*/
    }

    @Override
    protected int genererTemperature() {
        return strategie.generer();
    }
}
