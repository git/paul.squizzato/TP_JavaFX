package model;

import javafx.beans.InvalidationListener;
import javafx.beans.Observable;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.Property;
import javafx.beans.property.StringProperty;
import javafx.scene.control.SpinnerValueFactory;

public abstract class Capteur implements Runnable{
    protected StringProperty id;
    public String getId() {
        return id.get();
    }
    public StringProperty idProperty(){
        return id;
    }
    public void setId(String i){
        this.id.set(i);
    }

    protected StringProperty nom;
    public String getNom() {
        return nom.get();
    }
    public StringProperty nomProperty(){
        return nom;
    }
    public void setNom(String n){
        this.nom.set(n);
    }
    protected StringProperty valeur;
    public int getValeur(){
        return Integer.parseInt(valeur.get());
    }
    public StringProperty valeurProperty() {
        return valeur;
    }
    public void setValeur(int v) {
        this.valeur.set(Integer.toString(v));
    }

    protected IntegerProperty tpsGen;
    public int getTpsGen(){
        return tpsGen.getValue();
    }
    public IntegerProperty tpsGenProperty() {
        return tpsGen;
    }
    public void setTpsGen(int t) {
        this.tpsGen.setValue(t);
    }
    public boolean generer = false;
    protected abstract int genererTemperature();


    public void start(){
        generer=true;
    }
    public void stop(){
        generer=false;
    }
    public void run(){
        try {
            Thread.sleep(getTpsGen()*1000);
            if (generer){
                setValeur(genererTemperature());
            }
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
    }
}
