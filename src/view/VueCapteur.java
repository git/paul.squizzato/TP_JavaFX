package view;

import javafx.fxml.Initializable;
import model.Capteur;

import java.io.IOException;

public abstract class VueCapteur extends FXMLwindow implements Initializable{

    private Capteur capt;
    private Thread thread;
    public VueCapteur(Capteur capteur,String url) throws IOException {
        super(url, capteur.getNom());
        capt=capteur;
        //capt.start();
        thread=new Thread(capteur);
        thread.start();
    }
}
