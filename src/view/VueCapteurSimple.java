package view;

import javafx.beans.property.Property;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.text.Text;
import model.Capteur;
import model.CapteurSimple;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

public class VueCapteurSimple extends VueCapteur{
    @FXML
    private TextField nomCapteur;
    @FXML
    private Label idCapteur;

    @FXML
    private Label valeurCapteur;
    @FXML
    private Spinner<Integer> spinTps;
    Capteur capteur;

    public VueCapteurSimple(Capteur capteur) throws IOException {
        super(capteur,"views/CapteurSimple.fxml");
        this.capteur=capteur;
        idCapteur.textProperty().bindBidirectional(capteur.idProperty());

        nomCapteur.textProperty().bindBidirectional(capteur.nomProperty());

        valeurCapteur.textProperty().bindBidirectional(capteur.valeurProperty());

        spinTps.setValueFactory(new SpinnerValueFactory.IntegerSpinnerValueFactory(0,10,capteur.getTpsGen(),1));
        //capteur.tpsGenProperty().bind(spinTps.valueProperty());
    }


    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
    }
}
