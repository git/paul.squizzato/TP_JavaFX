package view;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.stage.Stage;
import model.Capteur;

import java.io.IOException;

public abstract class FXMLwindow extends Stage {
    public FXMLwindow(String url,String title) throws IOException {
        FXMLLoader loader = new FXMLLoader(getClass().getClassLoader().getResource(url));
        loader.setController(this);
        setTitle(title);
        setScene(new Scene(loader.load()));
        show();
    }
}
