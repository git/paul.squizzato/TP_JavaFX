package view;

import javafx.fxml.FXML;
import model.Capteur;

import java.io.IOException;
import javafx.scene.control.*;
import model.CapteurCompose;

import java.net.URL;
import java.util.ResourceBundle;

public class VueCapteurCompose extends VueCapteur{
    @FXML
    private TreeItem<Capteur> capteurs;
    public VueCapteurCompose(CapteurCompose capteur) throws IOException {
        super(capteur,"views/CapteurCompose.fxml");
    }
    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {

    }
}
